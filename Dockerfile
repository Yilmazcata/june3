FROM alpine:latest

# Install node
RUN apk update
RUN apk add nodejs

# Install Modules:
WORKDIR /app

# Copy in files:
COPY index.js .

EXPOSE 3000
CMD  ["node", "index.js"]

