# June 3



## Getting started

1. Update the files in your project.

2. Run the pipeline to build your docker image.

3. Pull the Docker Image (replace the command below with your image)

`docker pull registry.gitlab.com/acadeviatech-labs/june3:ab718fc7`

4. Run the application (replace the command below with your image).

`docker run -p 3000:3000 registry.gitlab.com/acadeviatech-labs/june3:ab718fc7`

5. In your web browser go to `http://localhost:3000`

6. Stop the application by killing the container in Docker Desktop.

7. Edit the text that the web application prints out by updating index.js and repeat the steps above.
